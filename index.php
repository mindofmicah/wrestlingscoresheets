<?php
use App\Factories\MatchFactory;
use App\Factories\MatchOrderFactory;
use App\Factories\ScoreableFactory;
use App\TextFileEventGateway;

require 'vendor/autoload.php';


$event = (new TextFileEventGateway(
    $scoreable_factory = new ScoreableFactory(),
    new MatchFactory($scoreable_factory),
    new MatchOrderFactory($scoreable_factory),
    file_get_contents('./data.txt'))
)->getEvent();

include('view.php');


function dd(...$args) {
    dump(...$args);
    exit();
}