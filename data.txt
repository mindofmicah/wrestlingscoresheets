Hell in a Cell
Alexa Bliss, Shayna Baszler
FallType
MatchDuration:10
Boolean:Will there be fire during the match?
WithinRange:Near falls, 1
---
Kevin Owens, Sami Zayn
FallType
WithinRange:Aerial Moves?, 1
Choice:Most aerial moves?,Owens, Zayn, Tie
Boolean:Will there be an entrance promo?
---
Cesaro,Seth Rollins
FallType
WithinRange:Near falls, 1
Choice:First to score a near fall?Rollins,Cesaro
MatchDuration:15