<?php
namespace App\Entities;

class MatchDurationScoreable extends BooleanScoreable
{
    /** @var int */
    private $limit;

    public function __construct(int $limit, int $point_value = 3)
    {
        parent::__construct('Will the match last longer than ' . $limit . ' minutes?', $point_value);
        $this->limit = $limit;
    }
}