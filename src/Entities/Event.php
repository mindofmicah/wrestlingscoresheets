<?php
namespace App\Entities;

use DateTime;

class Event
{
    /** @var string */
    private $name;
    /** @var DateTime */
    private $date;
    /** @var ScoreableGroup[] */
    private $scoreable_groups = [];

    public function __construct(string $name, DateTime $date)
    {
        $this->name = $name;
        $this->date = $date;
    }

    public function addScoreableGroup(ScoreableGroup $group): Event
    {
        $this->scoreable_groups[] = $group;
        return $this;
    }

    /**
     * @return Match[]
     */
    public function getMatches(): array
    {
        return array_values(array_filter($this->scoreable_groups, function ($group) {
            return $group instanceof Match;
        }));

    }

    public function getTitle(): string
    {
        return $this->name;
    }

    public function getFormattedDate(): string
    {
        return $this->date->format('m/d/Y');
    }

    /**
     * @return ScoreableGroup[]
     */
    public function getScoreableGroups(): array
    {
        return $this->scoreable_groups;
    }
}