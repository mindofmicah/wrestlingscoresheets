<?php
namespace App\Entities;

class FallTypeScoreable extends ChoiceScoreable
{
    /**
     * FallTypeScoreable constructor.
     *
     * @param string $prompt
     * @param int    $point_value
     */
    public function __construct(string $prompt, int $point_value = 3)
    {
        parent::__construct($prompt, ['Pinfall', 'Submission', 'DQ', 'Count-out', 'Other'], $point_value);
    }
}