<?php
namespace App\Entities;

class Match extends ScoreableGroup
{
    /** @var string[] */
    private $wrestlers;

    /**
     * Match constructor.
     *
     * @param string[] $wrestlers
     */
    public function __construct(array $wrestlers)
    {
        $this->wrestlers = $wrestlers;
    }

    public function getTitle(): string
    {
        return implode(' vs ', $this->wrestlers);
    }

    public function __toString()
    {
        return $this->getTitle();
    }
}