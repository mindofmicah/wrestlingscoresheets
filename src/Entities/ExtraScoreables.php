<?php
namespace App\Entities;

class ExtraScoreables extends ScoreableGroup
{
    /** @var string */
    private $heading;
    /** @var string */
    private $description;

    public function __construct(string $heading, string $description)
    {
        $this->heading = $heading;
        $this->description = $description;
    }

    public function getTitle(): string
    {
        return $this->heading;
    }
}