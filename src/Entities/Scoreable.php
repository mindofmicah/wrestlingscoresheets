<?php
namespace App\Entities;

class Scoreable
{
    /** @var string */
    protected $prompt;
    /** @var int */
    protected $point_value;

    public function __construct(string $prompt, int $point_value)
    {
        $this->prompt      = $prompt;
        $this->point_value = $point_value;
    }

    public function getPrompt(): string
    {
        return $this->prompt;
    }
}