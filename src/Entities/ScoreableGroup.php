<?php
namespace App\Entities;

abstract class ScoreableGroup
{
    /** @var Scoreable[] */
    protected $scoreables = [];

    public function addScoreable(Scoreable $scoreable): ScoreableGroup
    {
        $this->scoreables[] = $scoreable;

        return $this;
    }

    abstract public function getTitle(): string;

    /**
     * @return Scoreable[]
     */
    public function getScoreables(): array
    {
        return $this->scoreables;
    }
}