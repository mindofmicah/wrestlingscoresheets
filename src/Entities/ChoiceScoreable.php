<?php
namespace App\Entities;

class ChoiceScoreable extends Scoreable
{
    /** @var string[]|Match[] */
    private $choices;

    /**
     * ChoiceScoreable constructor.
     *
     * @param string           $prompt
     * @param string[]|Match[] $choices
     * @param int              $how_many
     */
    public function __construct(string $prompt, array $choices, int $how_many = 3)
    {
        parent::__construct($prompt, $how_many);
        $this->choices = $choices;
    }

    /**
     * @return Match[]|string[]
     */
    public function getChoices()
    {
        return $this->choices;
    }
}