<?php
namespace App\Entities;

class BooleanScoreable extends ChoiceScoreable
{
    public function __construct(string $prompt, int $point_value = 3)
    {
        parent::__construct($prompt, ['Yes', 'No'], $point_value);
    }
}