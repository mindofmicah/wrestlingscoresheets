<?php
namespace App\Entities;

class WithinRangeScoreable extends Scoreable
{
    /** @var int */
    private $checker;

    public function __construct(string $prompt, int $checker, int $point_value = 3)
    {
        parent::__construct($prompt . ' +/-' . $checker, $point_value);
        $this->checker = $checker;
    }
}