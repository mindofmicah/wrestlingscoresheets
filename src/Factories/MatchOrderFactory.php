<?php
namespace App\Factories;

use App\Entities\ExtraScoreables;
use App\Entities\Match;

class MatchOrderFactory
{
    /** @var ScoreableFactory */
    private $scoreable_factory;

    public function __construct(ScoreableFactory $scoreables)
    {
        $this->scoreable_factory = $scoreables;
    }

    /**
     * @param Match[] $matches
     * @param int[]   $extra_matches
     *
     * @return ExtraScoreables
     */
    public function createMatchOrderGroup(array $matches, array $extra_matches): ExtraScoreables
    {
        $misc = new ExtraScoreables('Main Card Match Position',
            'predict which match will air in each of the listed spots');

        $misc->addScoreable($this->scoreable_factory->createChoice('Opening Match', $matches));
        foreach ($extra_matches as $match) {
            $misc->addScoreable($this->scoreable_factory->createChoice('Match ' . $match, $matches));
        }
        $misc->addScoreable($this->scoreable_factory->createChoice('Main Event', $matches));

        return $misc;
    }
}