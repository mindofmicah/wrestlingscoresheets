<?php
namespace App\Factories;

use App\Entities\BooleanScoreable;
use App\Entities\ChoiceScoreable;
use App\Entities\FallTypeScoreable;
use App\Entities\Match;
use App\Entities\MatchDurationScoreable;
use App\Entities\WithinRangeScoreable;

class ScoreableFactory
{
    const DEFAULT_POINT_VALUE = 3;

    /**
     * @param string           $prompt
     * @param string[]|Match[] $choices
     *
     * @return ChoiceScoreable
     */
    public function createChoice(string $prompt, array $choices, int $point_value = self::DEFAULT_POINT_VALUE): ChoiceScoreable
    {
        return new ChoiceScoreable($prompt, $choices, $point_value);
    }

    public function createFallType(): FallTypeScoreable
    {
        return new FallTypeScoreable('Fall Type?', self::DEFAULT_POINT_VALUE);
    }

    public function createMatchDuration(int $min): MatchDurationScoreable
    {
        return new MatchDurationScoreable($min);
    }

    public function createBoolean(string $prompt): BooleanScoreable
    {
        return new BooleanScoreable($prompt);
    }

    public function createWithinRange(string $prompt, int $within): WithinRangeScoreable
    {
        return new WithinRangeScoreable($prompt, $within);
    }
}