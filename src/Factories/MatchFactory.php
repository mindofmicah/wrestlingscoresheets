<?php
namespace App\Factories;

use App\Entities\Match;

class MatchFactory
{
    /** @var ScoreableFactory */
    protected $scoreable_factory;

    public function __construct(ScoreableFactory $scoreable_factory)
    {
        $this->scoreable_factory = $scoreable_factory;
    }

    /**
     * @param string[] $wrestlers
     *
     * @return Match
     */
    public function create(array $wrestlers): Match
    {
        $match = new Match($wrestlers);
        $match->addScoreable($this->scoreable_factory->createChoice('Who is gonna win?', $wrestlers, 5));

        return $match;
    }
}