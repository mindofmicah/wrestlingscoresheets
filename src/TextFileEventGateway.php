<?php
namespace App;

use App\Entities\Event;
use App\Factories\MatchFactory;
use App\Factories\MatchOrderFactory;
use App\Factories\ScoreableFactory;
use DateTime;

class TextFileEventGateway
{
    /** @var ScoreableFactory */
    private $scoreable_factory;
    /** @var MatchFactory */
    private $match_factory;
    /** @var MatchOrderFactory */
    private $match_order_factory;
    /** @var string */
    private $file_contents;

    public function __construct(ScoreableFactory $scoreable_factory, MatchFactory $match_factory, MatchOrderFactory $match_order_factory, string $file_contents)
    {
        $this->scoreable_factory   = $scoreable_factory;
        $this->match_factory       = $match_factory;
        $this->match_order_factory = $match_order_factory;
        $this->file_contents       = $file_contents;
    }

    public function getEvent(): Event
    {
        $scoreable_factory   = $this->scoreable_factory;
        $match_factory       = $this->match_factory;
        $match_group_factory = $this->match_order_factory;

        $lines = array_map('trim', explode("\n", $this->file_contents));

        $title = array_shift($lines);
        $event = new Event($title, new DateTime);
        $match = null;
        foreach ($lines as $line) {
            if ($line === '---') {
                $match = null;
                continue;
            }

            if ($match === null) {
                $match = $match_factory->create($this->splitLine($line, ','));
                $event->addScoreableGroup($match);
                continue;
            }

            $chunks = $this->splitLine($line, ':');

            $type   = $chunks[0];
            $func   = "create{$chunks[0]}";
            $chunks = explode(',', $chunks[1] ?? '');
            if ($type === 'Choice') {
                $prompt = array_shift($chunks);
                $chunks = [$prompt, $chunks];
                unset($prompt);
            }

            $match->addScoreable($scoreable_factory->$func(...$chunks));
        }

        $event->addScoreableGroup($match_group_factory->createMatchOrderGroup($event->getMatches(), [3, 5]));

        return $event;
    }

    /**
     * @param string $line
     * @param string $delimiter
     *
     * @return string[]
     */
    private function splitLine(string $line, string $delimiter): array
    {
        return array_map('trim', (array)explode($delimiter, $line));
    }
}