<?php
use App\Entities\ChoiceScoreable;
use App\Entities\Event;

/** @var Event $event */
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Wrestling Scoresheet</title>
</head>
<body>
<h1><?= $event->getTitle() ?></h1>
<h2><?= $event->getFormattedDate() ?></h2>

<?php foreach ($event->getScoreableGroups() as $group_index => $group): ?>
    <div>
        <h3><?= $group->getTitle() ?></h3>
        <?php foreach ($group->getScoreables() as $scoreable_index => $scoreable): ?>
            <h4><?= $scoreable->getPrompt() ?></h4>
            <?php if ($scoreable instanceof ChoiceScoreable): ?>
                <?php foreach ($scoreable->getChoices() as $choice_index => $choice): ?>
                    <div>
                        <label><input type="radio" name="<?= $group_index ?>-<?= $scoreable_index ?>"/><?= $choice ?>
                        </label>
                    </div>
                <?php endforeach ?>
            <?php else: ?>
                <input type="text"/>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?php endforeach; ?>
</body>
</html>